﻿namespace war
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.yourlbl = new System.Windows.Forms.Label();
            this.oplbl = new System.Windows.Forms.Label();
            this.giveupbtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // yourlbl
            // 
            this.yourlbl.AutoSize = true;
            this.yourlbl.BackColor = System.Drawing.Color.Transparent;
            this.yourlbl.Font = new System.Drawing.Font("Papyrus", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.yourlbl.Location = new System.Drawing.Point(12, 9);
            this.yourlbl.Name = "yourlbl";
            this.yourlbl.Size = new System.Drawing.Size(136, 33);
            this.yourlbl.TabIndex = 1;
            this.yourlbl.Text = "Your score: ";
            // 
            // oplbl
            // 
            this.oplbl.AutoSize = true;
            this.oplbl.BackColor = System.Drawing.Color.Transparent;
            this.oplbl.Font = new System.Drawing.Font("Papyrus", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.oplbl.Location = new System.Drawing.Point(897, 9);
            this.oplbl.Name = "oplbl";
            this.oplbl.Size = new System.Drawing.Size(204, 33);
            this.oplbl.TabIndex = 2;
            this.oplbl.Text = "Opponent\'s score: ";
            // 
            // giveupbtn
            // 
            this.giveupbtn.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.giveupbtn.Font = new System.Drawing.Font("Papyrus", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.giveupbtn.Location = new System.Drawing.Point(18, 45);
            this.giveupbtn.Name = "giveupbtn";
            this.giveupbtn.Size = new System.Drawing.Size(108, 38);
            this.giveupbtn.TabIndex = 4;
            this.giveupbtn.Text = "Forfeit";
            this.giveupbtn.UseVisualStyleBackColor = false;
            this.giveupbtn.Click += new System.EventHandler(this.giveupbtn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::war.Properties.Resources.Daimyo_arrows_art;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1135, 531);
            this.Controls.Add(this.giveupbtn);
            this.Controls.Add(this.oplbl);
            this.Controls.Add(this.yourlbl);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label yourlbl;
        private System.Windows.Forms.Label oplbl;
        private System.Windows.Forms.Button giveupbtn;
    }
}

