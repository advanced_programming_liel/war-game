﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace war
{
    public partial class Form1 : Form
    {
        List<String> cards_paths = new List<String>();
        List<String> msg_cards = new List<String>();
        string[] symbols = { "clubs", "diamonds", "hearts", "spades"};
        string msg_to_server = string.Empty;
        string msg_from_server = string.Empty;
        List<PictureBox> gamecards = new List<PictureBox>();
        TcpClient client = new TcpClient();
        IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8820);
        int op_result = 0;
        int player_result = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            giveupbtn.Enabled = false;
            Thread t = new Thread(listenThread);
            ArrangingCards();

            System.Windows.Forms.PictureBox op = new PictureBox();
            op.Name = "opCard"; // in our case, this is the only property that changes between the different images
            op.Image = global::war.Properties.Resources.card_back_blue;
            op.Location = new Point(500, 20);
            op.Size = new System.Drawing.Size(100, 114);
            op.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;

            this.Controls.Add(op);

            gamecards.Add(op);

            t.Start();
        }

        public void GenerateCards()
        {
            Random rnd = new Random();
            Point nextLocation = new Point(21, 398);
            int index = 0;
            
            for (int i = 0; i < 10; i++)
            {
                System.Windows.Forms.PictureBox currentPic = new PictureBox();
                currentPic.Name = "card" + i; // in our case, this is the only property that changes between the different images
                currentPic.Image = global::war.Properties.Resources.card_back_red;
                currentPic.Location = nextLocation;
                currentPic.Size = new System.Drawing.Size(100, 114);
                currentPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
                
                this.Controls.Add(currentPic);

                gamecards.Add(currentPic);

                // calculate the location point for the next card to be drawn/added
                nextLocation.X += currentPic.Size.Width + 10;
                if (nextLocation.X > this.Size.Width)
                {
                    // move to next line below
                    nextLocation.X = 21;
                    nextLocation.Y += currentPic.Size.Height + 10;
                }

                currentPic.Click += delegate (object sender1, EventArgs e1)
                {
                    reset_cards_back();
                    //gamecards[0].Image = global::war.Properties.Resources.card_back_blue;
                    index = rnd.Next(0, cards_paths.Count - 1);
                    currentPic.ImageLocation = cards_paths[index];
                    msg_to_server = msg_cards[index];
                    player_is_playing();
                };
                
            }
        }

        private void reset_cards_back()
        {
            for(int i = 1; i < gamecards.Count; i++)
            {
                gamecards[i].Image = global::war.Properties.Resources.card_back_red;
            }
        }

        private void giveupbtn_Click(object sender, EventArgs e)
        {
            string end = "Your score: " + player_result + "\nOpponent's score: " + op_result;
            MessageBox.Show(end);
            msg_to_server = "2000";
            byte[] buffer = new byte[4];
            NetworkStream clientStream = client.GetStream();
            buffer = new ASCIIEncoding().GetBytes(msg_to_server);
            clientStream.Write(buffer, 0, buffer.Length);
            clientStream.Flush();
            this.Close();
        }

        private void listenThread()
        {
            string msg = " ";
            client.Connect(serverEndPoint);
            NetworkStream clientStream = client.GetStream();
            int index = 0;
            byte[] buffer = new byte[4];
            
            int bytesRead = clientStream.Read(buffer, 0, 4);
            msg = System.Text.Encoding.UTF8.GetString(buffer);
            if (msg[0] == '0')
            {
                Invoke((MethodInvoker)delegate { giveupbtn.Enabled = true; GenerateCards(); });
            }
            while (msg[0] != '2')
            {
                bytesRead = clientStream.Read(buffer, 0, 4);
                msg = System.Text.Encoding.UTF8.GetString(buffer);
                msg_from_server = msg;
                gamecards[0].Image = global::war.Properties.Resources.card_back_blue;
                if (msg[0] == '1')
                {
                    for(int i = 0; i < msg_cards.Count; i++)
                    {
                        if(msg_cards[i] == msg)
                        {
                            index = i;
                            break;
                        }
                    }
                    gamecards[0].ImageLocation = cards_paths[index];
                }
            }
            string end = "Your score: " + player_result + "\nOpponent's score: " + op_result;
            Invoke((MethodInvoker)delegate { this.Hide(); });
            MessageBox.Show(end);
            Invoke((MethodInvoker)delegate { this.Close(); });
        }

        private void player_is_playing()
        {
            byte[] buffer = new byte[4];
            NetworkStream clientStream = client.GetStream();

            buffer = new ASCIIEncoding().GetBytes(msg_to_server);
            clientStream.Write(buffer, 0, buffer.Length);
            clientStream.Flush();
            compere_cards();
        }

        private void compere_cards()
        {
            if((int.Parse(msg_to_server.Substring(1, 2)) == 1) || int.Parse(msg_to_server.Substring(1, 2)) > int.Parse(msg_from_server.Substring(1, 2)))
            {
                player_result++;
                op_result--;
            }
            else if(int.Parse(msg_to_server.Substring(1, 2)) < int.Parse(msg_from_server.Substring(1, 2)))
            {
                op_result++;
                player_result--;
            }

            yourlbl.Text = "Your score: " + player_result;
            oplbl.Text = "Opponent's score: " + op_result;
        }
        
        private void ArrangingCards()
        {
            string card = string.Empty;
            string finalCard = string.Empty;
            string path = string.Empty;
            string msgCard = string.Empty;
            for (int i = 1; i <= 13; i++)
            {
                msgCard += "1";
                if (i > 0 && i < 11)
                {
                    msgCard += "0";
                }
                msgCard += i.ToString();
                if (i == 1)
                {
                    card += "ace";
                }
                else if(i == 11)
                {
                    card += "jack";
                }
                else if (i == 12)
                {
                    card += "queen";
                }
                else if (i == 13)
                {
                    card += "king";
                }
                else
                {
                    card += i.ToString();
                }
                card += "_of_";
                for(int j = 0; j < symbols.Length; j++)
                { 
                    finalCard = card;
                    finalCard += symbols[j];
                    path = @"cards\" + finalCard + ".png";
                    cards_paths.Add(path);
                    msgCard += Char.ToUpper(symbols[j][0]);
                    msg_cards.Add(msgCard);
                    msgCard = msgCard.Remove(msgCard.Length - 1);
                }
                card = string.Empty;
                finalCard = string.Empty;
                path = string.Empty;
                msgCard = string.Empty;
            }
        }
    }
}
